const program = require('commander');
const copyFile = require('./file').copyFile;
const removeFile = require('./file').removeFile;
const createFile = require('./file').createFile;
const createConfigFile = require('./file').createConfigFile;
const svn = require('./svn').svn;
const myprocess = require('./process');

program
  .version(require('../package').version)
  .option('-q, --quduo', '执行趣多的打包')
  .option('-m, --meigaomei', '执行美高梅的打包')
  .parse(process.argv);

if (program.quduo) {
  const answer = {platform: "quduo"};
  const hash = {"quduo": "趣多", "meigaomei": "美高梅", "dongfanghui": "GIV国际"};
  const {platform} = answer;
  const platform_cn = hash[platform];

  try {
    removeFile();
    copyFile(platform);
    createFile(platform);
    createConfigFile(platform);
    myprocess();
    console.log(`${platform_cn}打包执行完毕(*^_^*)`);
    svn(platform);
    process.exit(0);
  } catch (e) {
    console.log(e);
    process.exit(0);
  }
}

if (program.meigaomei) {
  const answer = {platform: "meigaomei"};
  const hash = {"quduo": "趣多", "meigaomei": "美高梅", "dongfanghui": "GIV国际"};
  const {platform} = answer;
  const platform_cn = hash[platform];

  try {
    removeFile();
    copyFile(platform);
    createFile(platform);
    createConfigFile(platform);
    myprocess();
    console.log(`${platform_cn}打包执行完毕(*^_^*)`);
    svn(platform);
    process.exit(0);
  } catch (e) {
    console.log(e);
    process.exit(0);
  }
}
