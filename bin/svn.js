const {execSync} = require('child_process');
const path = require('path');
const {APP_PATH, SVN_QUDUO_URL, SVN_MEIGAO_URL, SVN_DONGFANG_URL} = require("./config");
const date = `${new Date().toLocaleDateString()} ${new Date().toLocaleTimeString()}`;

module.exports = {
  svn,
};

function svn(platform) {
  if (platform === 'quduo') {
    local_svn();
    console.log(`上传趣多svn完毕(*^_^*)`);
  }
  if (platform === 'meigaomei') {
    meigao_svn();
    console.log(`上传美高梅svn完毕(*^_^*)`);
  }
  if (platform === 'dongfanghui') {
    dongfang_svn();
    console.log(`上传GIV国际svn完毕(*^_^*)`);
  }
}

function local_svn() {
  let svn_url = SVN_QUDUO_URL;
  let win_url = path.join(APP_PATH, "./dist");
  let svn_delete = `删除svn的文件 => ${date}`;
  let svn_import = `移动h5的文件 = >${date}`;
  execSync(`svn delete -m "${svn_delete}" "${svn_url}/static"`);
  execSync(`svn delete -m "${svn_delete}" "${svn_url}/index.html"`);
  execSync(`svn import -m "${svn_import}" ${win_url}\\index.html ${svn_url}/index.html`);
  execSync(`svn import -m "${svn_import}" ${win_url}\\static ${svn_url}/static`);
}

function meigao_svn() {
  let svn_url = SVN_MEIGAO_URL;
  let win_url = path.join(APP_PATH, "./dist");
  let svn_delete = `删除svn的文件 => ${date}`;
  let svn_import = `移动h5的文件 = >${date}`;
  execSync(`svn delete -m "${svn_delete}" "${svn_url}/static"`);
  execSync(`svn delete -m "${svn_delete}" "${svn_url}/index.html"`);
  execSync(`svn import -m "${svn_import}" ${win_url}\\index.html ${svn_url}/index.html`);
  execSync(`svn import -m "${svn_import}" ${win_url}\\static ${svn_url}/static`);
}

function dongfang_svn() {
  let date = new Date().toLocaleDateString();
  let svn_url = SVN_DONGFANG_URL;
  let win_url = path.join(APP_PATH, "./dist");
  let svn_delete = `删除svn的文件 => ${date}`;
  let svn_import = `移动h5的文件 = >${date}`;
  execSync(`svn delete -m "${svn_delete}" "${svn_url}/static"`);
  execSync(`svn delete -m "${svn_delete}" "${svn_url}/index.html"`);
  execSync(`svn import -m "${svn_import}" ${win_url}\\index.html ${svn_url}/index.html`);
  execSync(`svn import -m "${svn_import}" ${win_url}\\static ${svn_url}/static`);
}

