const inquirer = require('inquirer');
const copyFile = require('./file').copyFile;
const removeFile = require('./file').removeFile;
const createFile = require('./file').createFile;
const createConfigFile = require('./file').createConfigFile;
const moveDist = require('./move').moveDist;
const svn = require('./svn').svn;
const myprocess = require('./process');

const questions = [
  {
    type: 'list',
    name: 'platform',
    message: '请选择要打包的平台',
    choices: [
      "quduo",
      'meigaomei',
      "dongfanghui",
    ],
  }
];

inquirer
  .prompt(questions)
  .then(async answer => {
      const hash = {"quduo": "趣多", "meigaomei": "美高梅", "dongfanghui": "GIV国际"};
      const {platform} = answer;
      const platform_cn = hash[platform];

      try {
        removeFile();
        copyFile(platform);
        createFile(platform);
        createConfigFile(platform);
        myprocess();
        // moveDist(platform);
        console.log(`${platform_cn}打包执行完毕(*^_^*)`);
        svn(platform);
        process.exit(0);
      } catch (e) {
        console.log(e);
        process.exit(0);
      }
    }
  );
