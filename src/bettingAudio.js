const { Howl, Howler } = require("howler");
let award = require("../static/award.wav");
let chat = require("../static/chat.mp3");
let notive = require("../static/notive.mp3");
let failure = require("../static/bet-failure.mp3");
let success = require("../static/bet_success.mp3");

export function perloadAudio() {
  // let body = document.getElementsByTagName('body')[0];
  //
  // [
  //   {id: 'award', src: award},
  //   {id: 'chat', src: chat},
  //   {id: 'notice', src: notice},
  //   {id: 'failure', src: failure},
  //   {id: 'success', src: success},
  // ].forEach(i => {
  //   let get_element = _createElement("audio", {
  //     id: `${i.id}Audio`,
  //     autoPlay: true,
  //     src: `${i.src}`,
  //     preload: "auto",
  //     // loop: true,
  //   });
  //
  //   if (!document.getElementById(`${i.id}Audio`)) {
  //     body.appendChild(get_element);
  //   }
  // });
}

function _createElement(ele, attrs) {
  let new_element = document.createElement(ele);

  for (let key in attrs) {
    if (!new_element.hasOwnProperty(key)) {
      new_element.setAttribute(key, attrs[key]);
    }
  }

  return new_element;
}

export function awardAudio() {
  _createAudio(award);
}

export function chatAudio() {
  _createAudio(chat);
}

export function notiveAudio() {
  _createAudio(notive);
}

export function failureAudio() {
  _createAudio(failure);
}

export function successAudio() {
  _createAudio(success);
}

function _createAudio(src) {
  let audio = localStorage.getItem("audio");
  if (audio && audio === "true") {
    let sound = new Howl({
      src: [src],
      autoplay: true
    });
    sound.play();
  }
}
