import Vue from "vue";
import Router from "vue-router";
// 组件
const Vlay = resolve => {
  import("../docs/index").then(module => {
    resolve(module);
  });
};
// 登陆
const Login = resolve => {
  import("components/login/login").then(module => {
    resolve(module);
  });
};
// 注册
const Reg = resolve => {
  import("components/reg/reg").then(module => {
    resolve(module);
  });
};
// 绑定手机号
const BindMobile = resolve => {
  import("components/reg/bindMobile").then(module => {
    resolve(module);
  });
};
//获取手机验证码
const GetCode = resolve => {
  import("components/reg/getCode").then(module => {
    resolve(module);
  });
};
// 注册条款
const Clause = resolve => {
  import("components/reg/clause").then(module => {
    resolve(module);
  });
};
// 忘记密码
const ForgotPwd = resolve => {
  import("components/reg/forgotPwd").then(module => {
    resolve(module);
  });
};
// 充值
const Recharge = resolve => {
  import("components/recharge/recharge").then(module => {
    resolve(module);
  });
};
// 线上充值
const OnLine = resolve => {
  import("components/recharge/onLine").then(module => {
    resolve(module);
  });
};
// 二维码支付
const CodePay = resolve => {
  import("components/recharge/codePay").then(module => {
    resolve(module);
  });
};
// 二维码支付
const SweepCode = resolve => {
  import("components/recharge/sweepCode").then(module => {
    resolve(module);
  });
};
// 二维码支付成功
const PaySec = resolve => {
  import("components/recharge/paySec").then(module => {
    resolve(module);
  });
};


// 银行转账
const Zf = resolve => {
  import("components/recharge/bank").then(module => {
    resolve(module);
  });
};
// 支付宝转账
const ZfbTrans = resolve => {
  import("components/recharge/alipay").then(module => {
    resolve(module);
  });
};
// 支付宝转账
const alipayHelp = resolve => {
  import("components/recharge/alipayHelp").then(module => {
    resolve(module);
  });
};
// 微信转账
const wxpayHelp = resolve => {
  import("components/recharge/wxpayHelp").then(module => {
    resolve(module);
  });
};
// 微信转账
const WxTrans = resolve => {
  import("components/recharge/wxpay").then(module => {
    resolve(module);
  });
};
// 未绑定银行卡
const UnBound = resolve => {
  import("components/index/withdrawCash/unBound").then(module => {
    resolve(module);
  });
};
// 添加银行卡
const AddCard = resolve => {
  import("components/index/withdrawCash/addCard").then(module => {
    resolve(module);
  });
};
// 添加银行卡
const MyCard = resolve => {
  import("components/index/withdrawCash/myCard").then(module => {
    resolve(module);
  });
};
// 选择银行卡
const CardList = resolve => {
  import("components/index/withdrawCash/cardList").then(module => {
    resolve(module);
  });
};
// 首页
const Main = resolve => {
  import("components/homes/home").then(module => {
    resolve(module);
  });
};


// 我的页面
const Personal = resolve => {
  import("components/personal/personal").then(module => {
    resolve(module);
  });
};
// 个人信息
const PersonalDetails = resolve => {
  import("components/personal/personalDetails").then(module => {
    resolve(module);
  });
};
// 金额明细
const Capitaldetails = resolve => {
  import("components/personal/capitaldetails").then(module => {
    resolve(module);
  });
};

// 客服
const KeFu = resolve => {
  import("components/index/main/kefu").then(module => {
    resolve(module);
  });
};

// 活动页面
const Promotion = resolve => {
  import("components/index/main/promotion").then(module => {
    resolve(module);
  });
};
// 活动详情
const PromotionDetail = resolve => {
  import("components/index/main/promotionDetail/promotionDetail").then(
    module => {
      resolve(module);
    }
  );
};
// 签到页面
const Signin = resolve => {
  import("components/index/main/promotionDetail/signin").then(module => {
    resolve(module);
  });
};

// 用户设置
const UserSetting = resolve => {
  import("components/personal/userSetting").then(module => {
    resolve(module);
  });
};
// 投注记录
const Betting = resolve => {
  import("components/personal/betting").then(module => {
    resolve(module);
  });
};
// 投注记录
const Zhuihao = resolve => {
  import("components/personal/zhuihao").then(module => {
    resolve(module);
  });
};
// 投注记录
const GamesList = resolve => {
  import("components/personal/gamesList").then(module => {
    resolve(module);
  });
};
// 投注详情
const BettingDetail = resolve => {
  import("components/personal/bettingDetail").then(module => {
    resolve(module);
  });
};
// 充值记录
const RechargeRecord = resolve => {
  import("components/personal/rechargeRecord").then(module => {
    resolve(module);
  });
};

// 充值详情
const Rechargebill = resolve => {
  import("components/personal/Rechargebill").then(module => {
    resolve(module);
  });
};

// 提现记录
const WithdrawRecord = resolve => {
  import("components/personal/withdrawRecord").then(module => {
    resolve(module);
  });
};
// 提现详情
const WithdrawDetail = resolve => {
  import("components/personal/withdrawDetail").then(module => {
    resolve(module);
  });
};
// 账变记录
const ZbRecord = resolve => {
  import("components/personal/zbRecord").then(module => {
    resolve(module);
  });
};
// 消息中心
const MsgCenter = resolve => {
  import("components/personal/msgCenter").then(module => {
    resolve(module);
  });
};
// 系统公告
const SystemMsgCenter = resolve => {
  import("components/personal/systemMsgCenter").then(module => {
    resolve(module);
  });
};
// 消息详情
const MsgDetail = resolve => {
  import("components/personal/msgDetail").then(module => {
    resolve(module);
  });
};
// 公告详情
const AnnouncementDetail = resolve => {
  import("components/personal/announcementDetail").then(module => {
    resolve(module);
  });
};
// 修改密码
const ModifyPwd = resolve => {
  import("components/personal/modifyPwd").then(module => {
    resolve(module);
  });
};
// 设置资金密码
const SetPwd = resolve => {
  import("components/personal/setPwd").then(module => {
    resolve(module);
  });
};

// 设置手机号码
// const BindMobile = resolve => {
//   import("components/personal/bindMobile").then(module => {
//     resolve(module);
//   });
// };

const Transfer = resolve => {
  import("components/transfer/transfer").then(module => {
    resolve(module);
  });
};

const Transfer_list = resolve => {
  import("components/transfer/transfer_list").then(module => {
    resolve(module);
  });
};

const Statement = resolve => {
  import("components/salary/statement").then(module => {
    resolve(module);
  });
};

// 游戏入口文件
const GamesMain = resolve => {
  import("components/games/gamesMain").then(module => {
    resolve(module);
  });
};
// 游戏分享页面
const Share = resolve => {
  import("components/games/share").then(module => {
    resolve(module);
  });
};
// 游戏入口文件
const GamesMainLiaoTian = resolve => {
  import("components/games/gamesMainLiaoTian").then(module => {
    resolve(module);
  });
};
// 预测走势页面
const TrendMain = resolve => {
  import("components/games/zoushi/trendMain").then(module => {
    resolve(module);
  });
};
// 时时彩预测走势页面
const awardResult = resolve => {
  import("components/games/zoushi/awardResult").then(module => {
    resolve(module);
  });
};
// PK10预测走势页面
const Pk10TrendMain = resolve => {
  import("components/games/zoushi/pk10TrendMain").then(module => {
    resolve(module);
  });
};
// 11选5预测走势页面
const Xuan5 = resolve => {
  import("components/games/zoushi/xuan5").then(module => {
    resolve(module);
  });
};
// PK10预测走势页面
const Pcegg = resolve => {
  import("components/games/zoushi/pcegg").then(module => {
    resolve(module);
  });
};
// 香港六合彩预测走势页面
const Markitem = resolve => {
  import("components/games/zoushi/markitem").then(module => {
    resolve(module);
  });
};
// 湖北快三游戏玩法说明
const ShuoMing = resolve => {
  import("components/games/wanfa/shuoming").then(module => {
    resolve(module);
  });
};
//购彩大厅
const Hall = resolve => {
  import("components/index/main//bettinghall/hall").then(module => {
    resolve(module);
  });
};

const AgencyCenter = resolve => {
  import("components/agent/agentIndex").then(module => {
    resolve(module);
  });
};

const CreateUserCenter = resolve => {
  import("components/agent/createAccount/createAccount").then(module => {
    resolve(module);
  });
};

const MyWater = resolve => {
  import("components/agencyCenter/myWater").then(module => {
    resolve(module);
  });
};

const MemberManage = resolve => {
  //import("components/agencyCenter/memberManage").then(module => {
  import("components/agent/team/memberManage").then(module => {
    resolve(module);
  });
};

const WaterReport = resolve => {
  import("components/agencyCenter/waterReport").then(module => {
    resolve(module);
  });
};

const Unknown = resolve => {
  import("components/404/404").then(module => {
    resolve(module);
  });
};

const Download = resolve => {
  import("components/download/download").then(module => {
    resolve(module);
  });
};

const AgentInfo = resolve => {
  import("components/agent/agencyInfo").then(module => {
    resolve(module);
  });
};
//上下级聊天
const ChatCenter = resolve => {
  import("components/agencyCenter/chatCenter").then(module => {
    resolve(module);
  });
};
const ChatPage = resolve => {
  import("components/agencyCenter/chatPage").then(module => {
    resolve(module);
  });
};
const TeamReport = resolve => {
  import("components/agencyCenter/teamReport").then(module => {
    resolve(module);
  });
};

const MaskDetail = resolve => {
  import("components/personal/maskDetail").then(module => {
    resolve(module);
  });
};
const QuickAdd = resolve => {
  import("components/agencyCenter/quickAdd").then(module => {
    resolve(module);
  });
};

//added router start by Ranka
const CreateLink = resolve => {
  import("components/agent/createAccount/addLink").then(module => {
    resolve(module);
  });
};

const CreateMember = resolve => {
  import("components/agent/createAccount/addMember").then(module => {
    resolve(module);
  });
};
const DailyWage = resolve => {
  import("components/agent/wage/dailyWage").then(module => {
    resolve(module);
  });
};

Vue.use(Router);

export default new Router({
  routes: [
    /*****************登录，注册*****************/
    {
      path: "/",
      redirect: '/login'
    },
    {
      path: "/lay",
      name: "lay",
      component: Vlay,
      meta: {
        title: "系统维护"
      }
    },
    {
      path: "/maskDetail",
      component: MaskDetail,
      meta: {
        title: "打码量详情"
      }
    },
    {
      path: "/404",
      name: "系统维护",
      component: Unknown,
      meta: {
        title: "系统维护"
      }
    },
    {
      path: "/download",
      name: "App下载",
      component: Download,
      meta: {
        title: "App下载"
      }
    },

    /*********************游戏入口************************* */
    {
      path: "/share",
      name: "Share",
      component: Share,
      meta: {
        title: "分享"
      }
    },
    {
      path: "/shuoming",
      name: "ShuoMing",
      component: ShuoMing,
      meta: {
        title: "湖北快三游戏规则说明"
      }
    },
    {
      path: "/trendMain",
      name: "TrendMain",
      component: TrendMain,
      meta: {
        title: "走势"
      }
    },
    {
      path: "/awardResult",
      name: "awardResult",
      component: awardResult,
      meta: {
        title: "时时彩走势"
      }
    },
    {
      path: "/xuan5",
      name: "Xuan5",
      component: Xuan5,
      meta: {
        title: "11选5走势"
      }
    },
    {
      path: "/pk10TrendMain",
      name: "Pk10TrendMain",
      component: Pk10TrendMain,
      meta: {
        title: "PK10走势"
      }
    },

    {
      path: "/pcegg",
      name: "Pcegg",
      component: Pcegg,
      meta: {
        title: "PC蛋蛋走势"
      }
    },
    {
      path: "/markitem",
      name: "Markitem",
      component: Markitem,
      meta: {
        title: "香港六合彩走势"
      }
    },

    {
      path: "/gamesMain",
      name: "GamesMain",
      component: GamesMain,
      meta: {
        title: "快三"
      }
    },
    {
      path: "/gamesMainLiaoTian",
      name: "GamesMainLiaoTian",
      component: GamesMainLiaoTian,
      meta: {
        title: "快三"
      }
    },
    {
      path: "/transfer",
      name: "Transfer",
      component: Transfer,
      meta: {
        title: "转账",
        keepAlive: true
      }
    },

    {
      path: "/transfer_list",
      component: Transfer_list,
      meta: {
        title: "游戏转账"
      }
    },
    {
      path: "/statement",
      component: Statement,
      meta: {
        title: "个人报表"
      }
    },
    {
      path: "/gamesList",
      component: GamesList,
      meta: {
        title: "游戏记录"
      }
    },
    {
      path: "/login",
      component: Login,
      meta: {
        title: "登录"
      }
    },
    {
      path: "/reg",
      component: Reg,
      meta: {
        title: "注册"
      }
    },
    {
      path: "/bindMobile",
      component: BindMobile,
      meta: {
        title: "绑定手机号"
      }
    },
    {
      path: "/getCode",
      component: GetCode,
      meta: {
        title: "修改资金密码"
      }
    },
    {
      path: "/clause",
      component: Clause,
      meta: {
        title: "注册条款"
      }
    },
    {
      path: "/forgotPwd",
      component: ForgotPwd,
      meta: {
        title: "忘记密码"
      }
    },
    /******************充值*******************/
    {
      path: "/recharge",
      component: Recharge,
      meta: {
        title: "充值"
      }
    },
    {
      path: "/onLine",
      component: OnLine,
      name: "onLine",
      meta: {
        title: "线上充值"
      }
    },
    {
      path: "/codepay",
      component: CodePay,
      name: "CodePay",
      meta: {
        title: "线上充值"
      }
    },
    {
      path: "/sweepCode",
      component: SweepCode,
      meta: {
        title: "扫码支付"
      }
    },
    {
      path: "/paySec",
      component: PaySec,
      name: "PaySec",
      meta: {
        title: "扫码支付"
      }
    },
    {
      path: "/bank",
      component: Zf,
      meta: {
        title: "银行卡转账"
      }
    },
    {
      path: "/alipay",
      component: ZfbTrans,
      name: "ZfbTrans",
      meta: {
        title: "支付宝转账"
      }
    },
    {
      path: "/wxpay",
      component: WxTrans,
      name: "WxTrans",
      meta: {
        title: "微信转账"
      }
    },
    /*********************提现********************/
    {
      path: "/unBound",
      component: UnBound,
      meta: {
        title: "提现"
      }
    },
    /*********************添加银行卡********************/
    {
      path: "/addCard",
      component: AddCard,
      name: "AddCard",
      meta: {
        title: "添加银行卡"
      }
    },
    /*********************我的银行卡********************/
    {
      path: "/myCard",
      component: MyCard,
      meta: {
        title: "我的银行卡"
      }
    },
    /*********************我的银行卡********************/
    {
      path: "/cardList",
      component: CardList,
      name: "CardList",
      meta: {
        title: "选择银行卡"
      }
    },
    /***********************福利详情**********************/
    {
      path: "/welfareDetail",
      component: (resolve) => {
        import("components/welfares/welfareDetail").then(module => {
          resolve(module);
        });
      },
      meta: {
        title: "发布帖子成功"
      }
    },
    /***********************发布帖子**********************/
    {
      path: "/publishPost",
      component: (resolve) => {
        import("components/forums/publishPost").then(module => {
          resolve(module);
        });
      },
      meta: {
        title: "发布帖子"
      }
    },
    /***********************发布帖子成功**********************/
    {
      path: "/publishSuccess",
      component: (resolve) => {
        import("components/forums/publishSuccess").then(module => {
          resolve(module);
        });
      },
      meta: {
        title: "发布帖子成功"
      }
    },
    /***********************主页**********************/
    {
      path: "/main",
      component: Main,
      children: [


        /*********************论坛详情************************* */
        {
          path: "/forumDetail",
          component: (resolve) => {
            import("components/forums/forumDetail").then(module => {
              resolve(module);
            });
          },
          meta: {
            title: "论坛详情"
          }
        },
        /******************我的首页****************** */
        {
          path: "/center",
          component: resolve => {
            import("components/homes/center/center").then(module => {
              resolve(module);
            });
          },
          meta: {
            title: "我的首页"
          }
        },
        /*********************论坛页面************************* */
        {
          path: "/forum",
          component: (resolve) => {
            import("components/homes/forum/Forum").then(module => {
              resolve(module);
            });
          },
          meta: {
            title: "论坛"
          }
        },
        {
          path: "/welfare",
          component: (resolve) => {
            import("components/homes/welfare/welfare").then(module => {
              resolve(module);
            });
          },
          meta: {
            title: "福利"
          }
        },
        {
          path: "/activity",
          component: (resolve) => {
            import("components/homes/activity/activity").then(module => {
              resolve(module);
            });
          },
          meta: {
            title: "活动"
          }
        },

        /*********************开奖页面************************* */
        {
          path: "/award",
          component: (resolve) => {
            import("components/homes/award/award").then(module => {
              resolve(module);
            });
          },
          meta: {
            title: "开奖"
          }
        },
        /*********************活动详情页面************************* */
        {
          path: "/promotionDetail",
          component: PromotionDetail,
          name: "PromotionDetail",
          meta: {
            title: "活动详情"
          }
        },
        /*********************购彩大厅************************* */
        {
          path: "/hall",
          name: "Hall",
          component: Hall,
          meta: {
            title: "购彩大厅"
          }
        },
        /********************客服****************** */
        {
          path: "/kefu",
          component: KeFu,
          meta: {
            title: "客服"
          }
        },
        /*******************我的页面******************** */
        {
          path: "/personal",
          component: Personal,
          meta: {
            title: "我的"
          }
        },
      ]
    },
    /*********************个人信息************************* */
    {
      path: "/personalDetails",
      component: PersonalDetails,
      name: "PersonalDetails",
      meta: {
        title: "个人信息"
      }
    },
    /*********************用户设置************************* */
    {
      path: "/userSetting",
      component: UserSetting,
      name: "UserSetting",
      meta: {
        title: "个人信息"
      }
    },
    /*********************资金明细************************* */
    {
      path: "/capitaldetails",
      component: Capitaldetails,
      name: "Capitaldetails",
      meta: {
        title: "金额明细"
      }
    },

    /*********************签到页面************************* */
    {
      path: "/signin",
      component: Signin,
      name: "Signin",
      meta: {
        title: "签到页面"
      }
    },
    /*********************投注页面************************* */
    {
      path: "/betting",
      component: Betting,
      meta: {
        title: "投注记录"
      }
    },
    /*********************投注页面************************* */
    {
      path: "/zhuihao",
      component: Zhuihao,
      meta: {
        title: "投注记录"
      }
    },

    /*********************充值记录************************* */
    {
      path: "/rechargeRecord",
      component: RechargeRecord,
      meta: {
        title: "充值记录"
      }
    },
    /*********************充值详情************************* */
    {
      path: "/Rechargebill",
      component: Rechargebill,
      meta: {
        title: "充值详情"
      }
    },
    /*********************充值帮助************************* */
    {
      path: "/alipayHelp",
      component: alipayHelp,
      meta: {
        title: "充值帮助"
      }
    },
    /*********************充值帮助************************* */
    {
      path: "/wxpayHelp",
      component: wxpayHelp,
      meta: {
        title: "充值帮助"
      }
    },
    /*********************提现记录************************* */
    {
      path: "/withdrawRecord",
      component: WithdrawRecord,
      meta: {
        title: "提现记录"
      }
    },
    /*********************提现详情************************* */
    {
      path: "/withdrawDetail",
      component: WithdrawDetail,
      name: "WithdrawDetail",
      meta: {
        title: "提现详情"
      }
    },
    /*********************账变记录************************* */
    {
      path: "/zbRecord",
      component: ZbRecord,
      meta: {
        title: "账变记录"
      }
    },
    /*********************消息中心************************* */
    {
      path: "/msgCenter",
      component: MsgCenter,
      meta: {
        title: "消息通知"
      }
    },
    {
      path: "/systemMsgCenter",
      component: SystemMsgCenter,
      meta: {
        title: "系统公告"
      }
    },
    /*********************消息详情************************* */
    {
      path: "/msgDetail",
      component: MsgDetail,
      name: "MsgDetail",
      meta: {
        title: "消息详情"
      }
    },
    /*********************公告详情************************* */
    {
      path: "/announcementDetail",
      component: AnnouncementDetail,
      name: "AnnouncementDetail",
      meta: {
        title: "公告详情"
      }
    },
    /*********************修改密码************************* */
    {
      path: "/modifyPwd",
      component: ModifyPwd,
      meta: {
        title: "修改密码"
      }
    },

    /*********************绑定手机号************************* */
    // {
    //   path: "/bindMobile",
    //   component: BindMobile,
    //   meta: {
    //     title: "绑定手机号"
    //   }
    // },
    {
      path: "/setPwd/",
      name: "setPwd",
      component: SetPwd,
      meta: {
        title: "设置资金密码"
      }
    },
    {
      path: "/chatPage",
      name: "ChatPage",
      component: ChatPage,
      meta: {
        title: "聊天页面"
      }
    },
    /*********************代理中心************************* */
    {
      path: "/agencyCenter",
      name: "agencyCenter",
      component: AgencyCenter,
      redirect: "/agentInfo",
      meta: {
        title: "代理中心"
      },
      children: [
        /*********************上下级聊天************************* */
        {
          path: "/chatCenter",
          name: "chatCenter",
          component: ChatCenter,
          meta: {
            title: "上下级聊天"
          }
        },

        /*********************代理信息************************* */
        {
          path: "/agentInfo",
          name: "agentInfo",
          component: AgentInfo,
          meta: {
            title: "代理信息"
          }
        },
        /*********************开户中心************************* */
        {
          path: "/createUserCenter",
          name: "createUserCenter",
          component: CreateUserCenter,
          meta: {
            title: "开户管理"
          }
        },
        /*********************返水报表************************* */
        {
          path: "/waterReport",
          name: "waterReport",
          component: WaterReport,
          meta: {
            title: "返水报表"
          }
        },
        {
          path: "/myWater",
          name: "myWater",
          component: MyWater,
          meta: {
            title: "我的返水"
          }
        },
        {
          path: "/memberManage",
          name: "memberManage",
          component: MemberManage,
          meta: {
            title: "成员管理"
          }
        },
        {
          path: "/teamReport",
          name: "TeamReport",
          component: TeamReport,
          meta: {
            title: "成员管理"
          }
        },
        {
          path: "/quickAdd",
          name: "QuickAdd",
          component: QuickAdd,
          meta: {
            title: "快速添加"
          }
        },
        {
          path: "/addLink",
          name: "CreateLink",
          component: CreateLink,
          meta: {
            title: "创建链接"
          }
        },
        {
          path: "/addMember",
          name: "CreateMember",
          component: CreateMember,
          meta: {
            title: "创建用户"
          }
        },
        {
          path: "/dailyWage",
          name: "DailyWage",
          component: DailyWage,
          meta: {
            title: "日工资"
          }
        },
      ]
    }
  ]
});

