## page

#### prop

| prop   |     作用      |  类型  | default |
| :----- | :-----------: | :----: | ------: |
| header | 是否有 border | Boolean |   false |
| footer| 是否有 footer | Boolean |   false |
